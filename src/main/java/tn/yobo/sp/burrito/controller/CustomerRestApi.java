package tn.yobo.sp.burrito.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.yobo.sp.burrito.model.Customer;
import tn.yobo.sp.burrito.service.CustomerService;

@RestController
@RequestMapping(value = "/customers")
public class CustomerRestApi {

	@Autowired
	private CustomerService customerService;

	@PostMapping(value ="/")
	public Customer create(@RequestBody Customer c) {
		return customerService.create(c);
	}
	
	@PutMapping(value ="/")
	public Customer update(@RequestBody Customer c) {
		return customerService.update(c);
	}
	
	@PostMapping(value ="/delete")
	public void delete(@RequestBody Customer c) {
		customerService.delete(c);
	}
	
	@DeleteMapping(value ="/{id}")
	public void deleteById(@PathVariable (value = "id")  Long c) {
		customerService.deleteById(c);
	}
	
	@GetMapping(value ="/")
	public List<Customer> findAll() {
		return customerService.findAll();
	}
	
	@GetMapping(value ="/{id}")
	public Customer findById(@PathVariable (value = "id") Long c) {
		return customerService.findById(c);
	}
	
	
}
