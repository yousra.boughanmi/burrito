package tn.yobo.sp.burrito.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.yobo.sp.burrito.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{
 
}
