package tn.yobo.sp.burrito.service;

import java.util.List;

import tn.yobo.sp.burrito.model.Customer;

//CRUD
public interface CustomerService {
 
	
	//Create
	Customer create(Customer c);
	
	//Update	
	Customer update(Customer c);
	
	//delete
	void delete(Customer c);
	
	//deleteById
	void deleteById(Long id);
	
	//findAll
	List<Customer> findAll();
	
	//findById
	Customer findById(Long id);
}
