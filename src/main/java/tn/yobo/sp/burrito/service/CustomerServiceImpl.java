package tn.yobo.sp.burrito.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.yobo.sp.burrito.model.Customer;
import tn.yobo.sp.burrito.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService
{
	@Autowired
	CustomerRepository customerRepository ;

	@Override
	public Customer create(Customer c) {
		return customerRepository.save(c);
	}

	@Override
	public Customer update(Customer c) {
		return customerRepository.save(c);
	}

	@Override
	public void delete(Customer c) {
		customerRepository.delete(c);		
	}

	@Override
	public void deleteById(Long id) {
		customerRepository.deleteById(id);		
	}

	@Override	
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}

	@Override
	public Customer findById(Long id) {
       Optional<Customer> customer = customerRepository.findById(id);
		return customer.isPresent() ? customer.get() : null;
	}

}
