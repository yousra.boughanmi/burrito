package tn.yobo.sp.burrito.model;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id ;
	private String fullName;
	private Date  birthDate;	
	private Boolean loyal ;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public Long getAge() {
		return ((new Date().getTime() - this.birthDate.getTime()) / 1000 / 60 / 60 / 24 / 365    );
	}
	
	public Boolean getLoyal() {
		return loyal;
	}
	public void setLoyal(Boolean loyal) {
		this.loyal = loyal;
	}
	
	
	
	
	
	
}
