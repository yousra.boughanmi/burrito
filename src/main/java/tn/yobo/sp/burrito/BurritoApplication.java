package tn.yobo.sp.burrito;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BurritoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BurritoApplication.class, args);
	}

}
