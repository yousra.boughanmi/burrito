package tn.yobo.sp.burrito;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tn.yobo.sp.burrito.model.Customer;
import tn.yobo.sp.burrito.service.CustomerService;

import java.util.Calendar;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceImplTests {
    @Autowired
    CustomerService customerService;

    @Test
    public void testAddCustomer() {
        List<Customer> customers = customerService.findAll();
        int expected = customers.size();
        Customer c = new Customer();
        //c.setId(1L);
        c.setFullName("Angelina JOOLIE");
        Calendar cal =Calendar.getInstance();
        cal.set(1992,05,01);
        c.setBirthDate(cal.getTime());
        c.setLoyal(true);
        Customer savedCustomer = customerService.create(c);
        Assert.assertEquals(expected + 1, customerService.findAll().size());
        Assert.assertNotNull(savedCustomer.getFullName());
    }
}
